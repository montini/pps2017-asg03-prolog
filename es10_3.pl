%3.1
% same(List1,List2)
% are the two lists the same?
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).


%3.2
% all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1
% example: all_bigger([10,20,30,40],[9,19,29,39]).

%all_bigger([],[]). % 1st attempt
all_bigger([X],[Y]) :- X > Y.
all_bigger([H1|T1],[H2|T2]) :- H1 > H2, all_bigger(T1,T2).

%3.3
% sublist(List1,List2)
% List1 should be a subset of List2
% example: sublist([1,2],[5,3,2,1]).
sublist([],X).
sublist([H1|T1],X) :- member(H1,X), sublist(T1, X).
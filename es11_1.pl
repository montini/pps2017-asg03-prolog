% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]) :- dropAny(X,Xs,L).

%1.1
% dropFirst(?Elem,?List,?OutList)
dropFirst(X,[X|T],T) :- !.
dropFirst(X,[H|T],[H|L]) :- dropFirst(X, T, L).

% dropLast(?Elem,?List,?OutList)
dropLast(X,[H|T],[H|L]) :- dropLast(X,T,L), !.
dropLast(X,[X|T],T).

% dropAll(?Elem,?List,?OutList)
dropAll(_, [], []) :- !.
dropAll(X,[X|T],Y) :- dropAll(X,T,Y), !.
dropAll(X,[H|T],[H|Y]) :- dropAll(X,T,Y).
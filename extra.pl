% zipIndex(List,Elem)
% example: zipIndex([a,b,c],[el(0,a),el(1,b),el(2,c)])
zipIndex(L,IL) :- zipIndex(L, 0, IL).
zipIndex([], N, []).
zipIndex([H|T],N,[el(N,H)|L]) :- C is N+1, zipIndex(T,C,L).


% replace(List, Elem1, Elem2, NewList) .
% example replace([a,b,a,c,d],a,z,[z,b,z,c,d]).
replace([], _, _, []).
replace([E1|T], E1, E2, [E2|L]) :- replace(T, E1, E2, L), !.
replace([H|T], E1, E2, [H|L]) :- replace(T, E1, E2, L).

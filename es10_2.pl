% size(List,Size)
% Size will contain the number of elements in List
size([],0).
size([_|T],M):- size(T,N), M is N+1.

%2.2
ssize([],zero).
ssize([_|T],s(C)):- ssize(T,C).

%2.3
% sum(List,Sum)
sum([],0).
sum([H|T],M):- sum(T,N), M is H + N.

% average(List,Average)
% it uses average(List,Count,Sum,Average)
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :-
C2 is C+1,
S2 is S+X,
average(Xs,C2,S2,A).

%average
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :-
C2 is C+1,
S2 is S+X,
average(Xs,C2,S2,A).


%2.5
% max(List,Max)
% Max is the biggest element in List
max([H|T], M) :- max(T, H, M).

max([],M,M).
max([H|T], Temp, M) :- H > Temp, max(T,H, M).
max([H|T], Temp, M) :- H =< Temp, max(T,Temp, M).
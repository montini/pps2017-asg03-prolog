search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

search2(X,[X,X|_]).
search2(X,[_|Xs]):-search2(X,Xs).

search_two(X,[X,_,X|_]).
search_two(X,[_|Xs]):-search_two(X,Xs).

search_anytwo(X,[X|T]):- search(X,T).
search_anytwo(X,[_|T]):- search_anytwo(X,T).
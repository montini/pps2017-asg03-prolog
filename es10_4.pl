%4.1
% seq(N,List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]) :- N > 0, N2 is N-1, seq(N2,T).


%4.2
% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).
seqR(0,[0]).
seqR(N,[N|T]) :- N > 0, C is N-1, seqR(C, T).


%4.3
% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).
last([],N,[N]).
last([H|T],N,[H|M]) :- last(T,N,M).

seqR2(0,[0]).
seqR2(N,L) :- N > 0, last(A,N,L), C is N-1, seqR2(C,A).

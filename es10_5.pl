%5.1
% inv(List,List)
% example: inv([1,2,3],[3,2,1]).
inv([],[]).
inv([H|T],L) :- inv(T,IL), append(IL,[H],L).

%5.2
% double(List,List)
% suggestion: remember predicate append/3
% example: double([1,2,3],[1,2,3,1,2,3]).
%double([],[]).
double(L,L2) :- append(L,L,L2).

%5.3
% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times(L,0,[]).
times(L,N,R) :- N > 0, C is N-1, times(L,C,LC), append(LC,L,R).

%5.4
% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj([],[]).
proj([[H|_]|T],L) :- proj(T,LT), append([H],LT,L).




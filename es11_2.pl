% fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]) :- fromList([H2|T],L).


% 2.2
% fromCircList(+List,-Graph)
fromCircList([X],[e(X,X)]).
fromCircList([H1,H2|T],G) :- append([H1,H2|T],[H1],L), fromList(L,G).


% dropAll(?Elem,?List,?OutList)
dropAll(_, [], []) :- !.
dropAll(X,[X|T],Y) :- dropAll(X,T,Y), !.
dropAll(X,[H|T],[H|Y]) :- dropAll(X,T,Y).

% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
% use dropAll defined in 1.1
% dropAll(?Elem,?List,?OutList)
dropNode(G,N,O) :- dropAll(e(N,_),G,G2), dropAll(e(_,N),G2,O).


%2.4
% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined
% with member(?Elem,?List)
reaching(G,N,L) :- findall(D, member(e(N,D),G), L).

%2.5
% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
anypath(G,O,D,L) :- member(e(O,B),G), anypath(G,B,D,NL), append([e(O,B)],NL,L).
anypath(G,O,D,L) :- member(e(O,D),G), append([], [e(O,D)], L), !.


%2.6
% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!
allreaching(G,N,L) :- findall(D, anypath(G,N,D,_), L).



